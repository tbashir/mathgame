//
//  Logic.swift
//  Images
//
//  Created by Tanveer Bashir on 5/2/15.
//  Copyright (c) 2015 Tanveer Bashir. All rights reserved.
//

import Foundation
import UIKit

struct Logic {
    var btn1:UIButton!
    var btn2:UIButton!
    var btn3:UIButton!
    var startButton:UIButton!
    var finishButton:UIButton!
    var imageView:UIImageView!

    func settingButtons(str:String) {
        switch str {
        case "math1":
        btn1.setTitle("10", forState: .Normal)
        btn2.setTitle("8", forState: .Normal)
        btn3.setTitle("7", forState: .Normal)
        case "math2":
        btn1.setTitle("11", forState: .Normal)
        btn2.setTitle("8", forState: .Normal)
        btn3.setTitle("9", forState: .Normal)
        case "math3":
        btn1.setTitle("15", forState: .Normal)
        btn2.setTitle("11", forState: .Normal)
        btn3.setTitle("14", forState: .Normal)
        case "math4":
        btn1.setTitle("13", forState: .Normal)
        btn2.setTitle("15", forState: .Normal)
        btn3.setTitle("16", forState: .Normal)
        case "math5":
        btn1.setTitle("14", forState: .Normal)
        btn2.setTitle("12", forState: .Normal)
        btn3.setTitle("10", forState: .Normal)
        case "math6":
        btn1.setTitle("11", forState: .Normal)
        btn2.setTitle("13", forState: .Normal)
        btn3.setTitle("7", forState: .Normal)
        case "math7":
        btn1.setTitle("17", forState: .Normal)
        btn2.setTitle("14", forState: .Normal)
        btn3.setTitle("19", forState: .Normal)
        case "math8":
        btn1.setTitle("50", forState: .Normal)
        btn2.setTitle("5", forState: .Normal)
        btn3.setTitle("0", forState: .Normal)
        case "math9":
        btn1.setTitle("11", forState: .Normal)
        btn2.setTitle("10", forState: .Normal)
        btn3.setTitle("13", forState: .Normal)
        case "math10":
        btn1.setTitle("0", forState: .Normal)
        btn2.setTitle("2", forState: .Normal)
        btn3.setTitle("3", forState: .Normal)
        case "math11":
        btn1.setTitle("3", forState: .Normal)
        btn2.setTitle("0", forState: .Normal)
        btn3.setTitle("4", forState: .Normal)
        case "math12":
        btn1.setTitle("6", forState: .Normal)
        btn2.setTitle("5", forState: .Normal)
        btn3.setTitle("16", forState: .Normal)
        case "math13":
        btn1.setTitle("7", forState: .Normal)
        btn2.setTitle("10", forState: .Normal)
        btn3.setTitle("9", forState: .Normal)
        case "math14":
        btn1.setTitle("0", forState: .Normal)
        btn2.setTitle("9", forState: .Normal)
        btn3.setTitle("13", forState: .Normal)
        default:
        btn1.setTitle("?", forState: .Normal)
        btn2.setTitle("?", forState: .Normal)
        btn3.setTitle("?", forState: .Normal)
        }
    }

    func cnhageButtonTitleAndState(){
        disableUserInteractionOnButtons()
        startButton.hidden = false
        finishButton.hidden = true
    }

    func disableUserInteractionOnButtons(){
        btn1.userInteractionEnabled = false
        btn2.userInteractionEnabled = false
        btn3.userInteractionEnabled = false
    }

    func enableUserInteractionOnButton(){
        btn1.userInteractionEnabled = true
        btn2.userInteractionEnabled = true
        btn3.userInteractionEnabled = true
    }

    func animateLabel(scoreLabel: UILabel){
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: nil, animations:{
            scoreLabel.center = CGPoint(x: 143 , y: 374) }, completion: nil)
    }
}