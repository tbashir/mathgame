//
//  Math.swift
//  Images
//
//  Created by Tanveer Bashir on 5/8/15.
//  Copyright (c) 2015 Tanveer Bashir. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class Math {
    var mathModel:Logic!
    var wrong:Int?
    var imageIndex:Int?
    var score:Int?
    var str:String?
    var cards:[String]?
    var answerDict:[String:Int]?
    var wrongAnswers:[String] = []
    var dictionaryOfAnswerAsString:[String:String]?
    var wrongAnswerString:String = ""
    var audioPlayer:AVAudioPlayer!

    init(){
        self.cards = setArray()
        self.answerDict = setDict()
        self.dictionaryOfAnswerAsString = setDictionaryOfAnswerAsString()
    }

    //MARK: - Private func's
    private func setArray() -> [String]{
        var cards = [
            "math1","math2","math3","math4",
            "math5","math6", "math7", "math8",
            "math9","math10","math11", "math12",
            "math13", "math14","math"]
        return cards
    }

   private func setDict() -> [String:Int]{
        var answerDict = [
            "math1": 8,"math2": 9,"math3": 14,"math4": 15,
            "math5": 12, "math6": 13, "math7": 17, "math8": 5,
            "math9": 11, "math10": 2, "math11": 3, "math12": 5,
            "math13": 7, "math14": 9]
        return answerDict
    }

   private func setDictionaryOfAnswerAsString() -> [String:String]{
        var dictionaryOfAnswerAsString = [
            "math1": "6 + 2", "math2": "5 + 4", "math3": "9 + 5", "math4": "9 + 6",
            "math5": "4 + 8", "math6": "6 + 7", "math7": "8 + 9", "math8": "5 + 0",
            "math9": "6 + 5", "math10": "0 + 2", "math11": "0 + 3", "math12": "2 + 3", "math13": "0 + 7", "math14": "5 + 4"]
        return dictionaryOfAnswerAsString
    }
    
    //MARK: - Public func's
    func audioFiles(file:String){
        let audioFile = NSBundle.mainBundle().URLForResource(file, withExtension: "wav")
        audioPlayer = AVAudioPlayer(contentsOfURL: audioFile, error: nil)
        audioPlayer.prepareToPlay()
    }

    func answerLogic(num:Int, correctLabel:UILabel, wrongLabel:UILabel){
        if num == answerDict![str!]{
            audioFiles("pop")
            audioPlayer.play()
            score!++
            correctLabel.text = "\(score!)"
        } else {
            audioFiles("SpringBong")
            audioPlayer.play()
            wrong!++
            wrongLabel.text = "\(wrong!)"
        }
    }

    func finishLogic(imageView:UIImageView){
        if imageIndex! == cards!.count{
            imageIndex! = 0
        }
        var finalScore = score!
        
// Alert works if declared in ViewController
/*        var alert = UIAlertController(title: "You have completed your task", message:              "Your score is \(finalScore)", preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel , handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
*/
        mathModel.cnhageButtonTitleAndState()
        if score! > 10 {
            imageView.image = UIImage(named: "goodJob")
            audioFiles("clap")
            audioPlayer.play()
        } else {
            imageView.image = UIImage(named: "sorry")
        }
    }

    func showImage(imageView:UIImageView,sButton:UIButton, fButton:UIButton){
        sButton.hidden = true
        if imageIndex < cards!.count {
            str = cards?[imageIndex!]
            imageView.image = UIImage(named: "\(cards![imageIndex!])")
            imageView.accessibilityLabel = "\(cards![imageIndex!])"
            imageIndex! += 1
            if imageView.accessibilityLabel == cards?.last {
                imageView.image = UIImage(named: "complete")
                fButton.hidden = false
                sButton.hidden = true
                mathModel.disableUserInteractionOnButtons()
            }
        }
        mathModel.settingButtons(str!)
    }
}