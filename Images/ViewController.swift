//
//  ViewController.swift
//  Images
//
//  Created by Tanveer Bashir on 5/1/15.
//  Copyright (c) 2015 Tanveer Bashir. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var wrongAnswerLabel: UILabel!
    @IBOutlet weak var correctAnswerLabel: UILabel!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var stopButton: UIButton!
    let math = Math()

    override func viewDidLoad() {
        super.viewDidLoad()
        math.imageIndex = 0
        math.score = 0
        math.wrong = 0
        setButtonRadius()
        math.mathModel = Logic(btn1:btn1, btn2:btn2, btn3:btn3, startButton:startButton, finishButton:finishButton, imageView:imageView)
        math.mathModel.disableUserInteractionOnButtons()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }

    //MARK: - Answer Button
    @IBAction func answerButton(sender: UIButton) {
        var title = sender.titleLabel?.text!
        if let num = title?.toInt() {
        math.answerLogic(num, correctLabel:correctAnswerLabel, wrongLabel: wrongAnswerLabel)
        }
        math.showImage(imageView, sButton:startButton, fButton:finishButton)
        scoreLabel.text = "\(math.score!)"
    }

    @IBAction func startGame(sender: UIButton){
        stopButton.hidden = true
        startButton.hidden = true
        math.mathModel.enableUserInteractionOnButton()
        math.showImage(imageView, sButton:startButton, fButton:finishButton)
        reset()
    }

    @IBAction func finsihButton(sender: UIButton){
        math.finishLogic(imageView)
    }

    func setButtonRadius(){
        btn1.layer.cornerRadius = 15
        btn2.layer.cornerRadius = 15
        btn3.layer.cornerRadius = 15
        startButton.layer.cornerRadius = 15
        finishButton.layer.cornerRadius = 15
        stopButton.layer.cornerRadius = 15
    }

    func reset(){
        math.score! = 0
        math.wrong! = 0
        wrongAnswerLabel.text = "\(0)"
        correctAnswerLabel.text = "\(0)"
        scoreLabel.text = "\(0)"
    }
}

